import re
import time

from selenium import webdriver
from selenium.common import TimeoutException, NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

from load_django import *
from core.models import *


class ProductInfo:
    def __init__(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("start-maximized")
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--blink-settings=imagesEnabled=false")
        chrome_options.add_argument("--disable-javascript")
        chrome_options.add_argument("--disable-plugins")
        chrome_options.add_argument("--disable-extensions")
        self.driver = webdriver.Chrome(options=chrome_options)
        self.links = ProductLink.objects.filter(status=False)

    def product_link(self, link):
        self.driver.get(link)

    def find_price(self):
        try:
            price = self.driver.find_element(
                By.XPATH, "//span[@class='pdpprice-row1-reg-price pdpprice-row1-reg-price-striked']").text
            sale_price = self.driver.find_element(
                By.XPATH, "//span[@class='pdpprice-row2-main-text pdpprice-row2-main-text-red']").text

            price = float(price.split("$")[1].split()[0])
            sale_price = float(sale_price.split("$")[1])
            return sale_price, price
        except NoSuchElementException:
            price = self.driver.find_element(By.XPATH, "//meta[@itemprop='price']").get_attribute("content")
            return 0, float(price)

    def get_product_info(self):
        sale_price, price = self.find_price()

        # sometime the reviews is not loaded, so we need to refresh the page
        try:
            find_reviews = WebDriverWait(self.driver, 10).until(
                ec.presence_of_element_located((By.XPATH, "//div[@class='bv_numReviews_text']"))).text
            reviews = ''.join(filter(str.isdigit, find_reviews))
        except TimeoutException:
            self.driver.refresh()
        finally:
            find_reviews = WebDriverWait(self.driver, 10).until(
                ec.presence_of_element_located((By.XPATH, "//div[@class='bv_numReviews_text']"))).text
            reviews = ''.join(filter(str.isdigit, find_reviews))

        title = self.driver.find_element(By.XPATH, "//h1[@class='product-title']").text.strip()
        img = self.driver.find_element(By.XPATH, "//div[@class='pdp-image'] //a").get_attribute("href")
        brand = self.driver.find_element(By.XPATH, "//meta[@itemprop='brand']").get_attribute("content")

        get_upc = self.upc_finder()

        defaults = {
            "price": price,
            "discount_price": sale_price,
            "reviews": int(reviews),
            "brand": brand,
            "photo_link": img,
            "upc_code": get_upc,
        }

        obj, created = Product.objects.get_or_create(title=title, defaults=defaults)

    def update_status(self, link):
        link.status = True
        link.save()

    def upc_finder(self):
        js_element = self.driver.find_element(By.XPATH, "//script[contains(text(),'UPC')]")
        get_upc_element = js_element.get_attribute('innerHTML')
        reg = r'"UPC":\{"image":null,"ID":"(.+?)"\}'
        upc_code = re.search(reg, get_upc_element)
        return upc_code.group(1)

    def main(self):
        for link in self.links:
            self.product_link(link.link)
            self.get_product_info()
            self.update_status(link)
        self.driver.close()


if __name__ == '__main__':
    ProductInfo().main()
