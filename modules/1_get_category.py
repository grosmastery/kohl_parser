from django.db import IntegrityError
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

from load_django import *
from core.models import *


class CategoryClass:

    def __init__(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("start-maximized")
        self.driver = webdriver.Chrome(options=chrome_options)
        self.driver.get("https://www.kohls.com/")

    def get_categories(self):
        self.driver.find_element(By.XPATH, "//span[@class='middle-menu-title']").click()

        categories = WebDriverWait(self.driver, 10).until(ec.presence_of_all_elements_located(
            (By.XPATH, "//li[@class='nav-group'] //a")))

        for category in categories:
            try:
                obj, created = Category.objects.get_or_create(
                    title=category.get_attribute('textContent').strip(), link=category.get_attribute('href'))
            except IntegrityError:
                pass
        self.driver.close()


if __name__ == '__main__':
    CategoryClass().get_categories()
