import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

from load_django import *
from core.models import *


class Link:

    def __init__(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("start-maximized")
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--blink-settings=imagesEnabled=false")
        chrome_options.add_argument("--disable-javascript")
        chrome_options.add_argument("--disable-plugins")
        chrome_options.add_argument("--disable-extensions")
        self.driver = webdriver.Chrome(options=chrome_options)
        self.links = Category.objects.filter(status=False)

    def category_link(self, link):
        self.driver.get(link)

    def next_page(self):
        WebDriverWait(self.driver, 3).until(ec.element_to_be_clickable(
            (By.XPATH, "//img[@title='Next Page']"))).click()

    def get_product_links_from_page(self, category):
        time.sleep(2)
        products = self.driver.find_elements(By.XPATH, "//div[@class='prod_img_block'] //a")
        products_title = self.driver.find_elements(By.XPATH, "//div[@class='prod_nameBlock'] //p")

        for product, title in zip(products, products_title):
            defaults = {
                "title": title.text,
                "category": category,
            }
            obj, created = ProductLink.objects.get_or_create(link=product.get_attribute('href'), defaults=defaults)

    def get_all_product_links(self, category):
        counter = WebDriverWait(self.driver, 3).until(ec.presence_of_element_located(
                (By.XPATH, "//input[@class='pageNum pageInput']"))).get_attribute('max')
        for _ in range(int(counter)):
            self.get_product_links_from_page(category)
            self.next_page()

    def update_status(self, link):
        link.status = True
        link.save()

    def main(self):
        for link in self.links:
            self.category_link(link.link)
            self.get_all_product_links(link)
            self.update_status(link)
        self.driver.close()


if __name__ == '__main__':
    Link().main()
