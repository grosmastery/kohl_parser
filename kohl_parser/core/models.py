from django.db import models


class Category(models.Model):
    title = models.CharField(max_length=255)
    link = models.TextField(unique=True)
    status = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Category"


class ProductLink(models.Model):
    category = models.ForeignKey('core.Category', on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    link = models.TextField(unique=True)
    status = models.BooleanField(default=False)

    def __str__(self):
        return self.title.title

    class Meta:
        verbose_name_plural = "ProductLink"


class Product(models.Model):
    title = models.CharField(max_length=255)
    price = models.FloatField(default=0)
    discount_price = models.FloatField(default=0)
    reviews = models.IntegerField(default=0)
    brand = models.CharField(max_length=255)
    photo_link = models.CharField(max_length=255)
    upc_code = models.CharField(max_length=255)

    class Meta:
        verbose_name_plural = "Product"
