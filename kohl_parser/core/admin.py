from django.contrib import admin
from .models import *


@admin.register(Category)
class NewsAdmin(admin.ModelAdmin):
    list_display = ("title", "status", "link")


@admin.register(ProductLink)
class ProductLinkAdmin(admin.ModelAdmin):
    list_display = ("title", "category", "link", "status")


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ("title", "upc_code", "price", "discount_price", "reviews", "brand", "photo_link")
