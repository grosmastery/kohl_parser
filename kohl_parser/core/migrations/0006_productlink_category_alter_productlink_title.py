# Generated by Django 4.2.2 on 2023-06-27 09:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_product_upc_code'),
    ]

    operations = [
        migrations.AddField(
            model_name='productlink',
            name='category',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='core.category'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='productlink',
            name='title',
            field=models.CharField(max_length=255),
        ),
    ]
